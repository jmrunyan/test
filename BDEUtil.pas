unit BDEUtil;

interface

uses
  TUTIL32, BDE, DBTables;

type

  TOnCallBack = reference to procedure (ecbType: CBType; Data: LongInt; pcbInfo: Pointer);

  TBDEUtil = class
  private
    CbInfo: TUVerifyCallback;
    TUProps: CURProps;
    hDb: hDBIDb;
    vhTSes: hTUSes;
    FOnCallBack: TOnCallBack;

  public
    constructor Create;
    destructor Destroy; override;

    function GetTCursorProps(szTable: String): Boolean;
    procedure RegisterCallBack;
    procedure UnRegisterCallBack;

    property OnCallback: TOnCallBack read FOnCallBack write FOnCallBack;
  end;

implementation

function GenProgressCallBack(ecbType: CBType; Data: LongInt; pcbInfo: Pointer):
  CBRType; stdcall;
var
  CBInfo: TUVerifyCallBack;
begin
  CBInfo := TUVerifyCallBack(pcbInfo^);
  if ecbType = cbGENPROGRESS then
    case CBInfo.Process of
     TUVerifyHeader: begin
       MainForm.PBHeader.Position := CBInfo.percentdone;
     end;
     TUVerifyIndex: begin
       MainForm.PBIndexes.Position := CBInfo.percentdone;
     end;
     TUVerifyData: begin
       MainForm.PBData.Position := CBInfo.percentdone;
     end;
     TURebuild: begin
       MainForm.PBRebuild.Position := CBInfo.percentdone;
     end;
    end;

  Result := cbrUSEDEF;
end;


{ TBDEUtil }

constructor TBDEUtil.Create;
begin
  Check(TUInit(vhtSes));
end;

destructor TBDEUtil.Destroy;
begin
  Check(TUExit(vhtSes));
  inherited Destroy;
end;

function TBDEUtil.GetTCursorProps(szTable: String): Boolean;
begin
  if TUFillCURProps(vHtSes, PChar(szTable), TUProps) = DBIERR_NONE then
    Result := True
  else Result := False;
end;

procedure TBDEUtil.RegisterCallback;
begin
 Check(DbiRegisterCallBack(nil, cbGENPROGRESS, 0,
            sizeof(TUVerifyCallBack), @CbInfo, GenProgressCallback));
end;

procedure TBDEUtil.UnRegisterCallback;
begin
  Check(DbiRegisterCallBack(nil, cbGENPROGRESS, 0,
           sizeof(TUVerifyCallBack), @CbInfo, nil));
end;

end.
 